**Table of content**
1. What is the approach?
2. What is the approach followed?
3. Problem faced while using the approach
4. Step to run code

**The approach that i used**
1. I understand the material about sockets at https://socket.io/get-started/chat
2. I tried the tutorial on the link https://socket.io/get-started/chat
3. I tried to change it from express to hapi by displaying "Hello World" first
4. look for ways to display files on hapi at https://hapi.dev/tutorials/servingfiles/?lang=en_US files displayed index.html
5. Read and understand the code for sending files at https://github.com/socketio/socket.io/tree/master/examples/chat
6. Make it according to the code in the link https://github.com/socketio/socket.io/tree/master/examples/chat
7. Create 2 contentner to create a channel that connects one for the page class and the other for the channel in purple in index.html and makes the style in the style.css file

**The reason why i use that way to create my project**
Because I don't understand the plot a bit

**The problem that I experienced**
1. Incorrectly entered the name of the folder to hold the index.html, main.js, and style.css files
2. unable to make the channel

**Step to run**
1. run the index.js node
2. then open the localhost link: 1515